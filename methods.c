#include "methods.h"
#include <signal.h>
#include <errno.h>

int isSpaceOnly(char *line)
{
    for(int i =0; i < strlen(line); i++)
        if(line[i] != ' ')
            return 0;

    return 1;
}

void ControlCHandle(int sig_num)
{
    signal(SIGINT, ControlCHandle);
    printf("\n");
    fflush(stdout);
}

int getDollarIndex(char *text)
{
    for(int i = 1; i < strlen(text); i++)
    {
        if(text[i]== '$' && text[i-1] != '\\')
            return i;
    }
    return 0;
}

//Method to return the variable value of the variable name passed
char *getVariables(char *var_name)
{
    //string used to store the variable name without the $ in front
    char var[100];
    //getting the variable name
    substring(var_name, var, 2, strlen(var_name));

    if(getenv(var) != NULL)
        return getenv(var);
    else
        return var_name;
}

/**
 * Method to return the type of input or output operator asked in the user input
 * @param string: the command entered by the user
 * @return  0: If no operator is passed
 *          1: '>'
 *          2: '>>'
 *          3: '<'
 *          4: '<<<'
 */
int opType(char *string)
{
    //loop through every character
    for(int i = 0; i < strlen(string); i++)
    {
        //if first character, it will not be escaped for sure
        if(i==0)
        {
            if(string[i]== '>')
            {
                //if next element is the same
                if (string[i + 1] == '>')
                    return 2;
                else
                    return 1;
            }

            else if(string[i]== '<')
            {
                //if next element is the same
                if(string[i+1]== '<')
                {
                    //if next element is the same
                    if (string[i + 2] == '<')
                        return 4;
                }
            }
        }
        else //If not the first character, we should make sure that the character is not escaped
        {
            //If character is equal to '>' and is not escaped in the previous character
            if(string[i]== '>' && string[i-1] !='\\')
            {
                if (string[i + 1] == '>')
                    return 2;
                else
                    return 1;
            }
                //If character is equal to '<' and is not escaped in the previous character
            else if(string[i]== '<' && string[i-1] !='\\')
            {
                if(string[i+1]== '<')
                {
                    if (string[i + 2] == '<')
                        return 4;
                }
                else
                {
                    return 3;
                }

            }
        }

    }
    return 0;

}

/*
 * Method to return the index of where the operator is in the input
 */
int opIndex(char *string)
{
    for(int i = 0; i < strlen(string); i++)
    {
        if(i <= strlen(string)-2)
            if(string[i]== '>' || (string[i]== '>' && string[i+1]== '>') || string[i]== '<' || (string[i]== '<' && string[i+1]=='<' && string[i+2] == '<'))
                return i;
    }
    return -1;

}

//Method to check whether a string includes an equals in it. This is used for variable assignment
int hasEquals(char *string)
{
    for(int i = 0; i < strlen(string); i++)
    {
        if(string[i]== '=')
            return 1;
    }
    return 0;
}

//Checks if the string contains a pipe operator
int hasPipe(char *string)
{
    for(int i = 0; i < strlen(string); i++)
    {
        //If it is the first character it will not be escaped for sure
        if(i==0)
        {
            if (string[i] == '|')
                return 1;
        }
            //If it is not the first character, it means that it might be escaped
        else
        {
            if (string[i] == '|' && string[i-1] != '\\')
                return 1;
        }

    }
    return 0;
}

/**
 * Method to substring the string into another string
 * Positions start from 1 not 0
 * @param initial_string
 * @param new_string
 * @param position_to_start
 * @param position_to_end
 */
void substring(char initial_string[], char new_string[], int position_to_start, int position_to_end)
{
    int counter = 0;

    //loop while counter is smaller than index to stop at
    while (counter < position_to_end)
    {
        new_string[counter] = initial_string[position_to_start + counter - 1];
        counter++;
    }
    new_string[counter] = '\0';
}

/**
 * Method which performs the operation according to the choice made by the user
 * @param line- String which the user inputs
 */
void performTask(char *line)
{

    //variable which stores the type of input/output operand
    int op_type;
    //variable which stores the starting position of the operand
    int op_index;
    //variable which stores the length of the operand
    int op_length;
    //variable which will be used to store command in case of use of an input/output operand
    char command[1000];
    //variable which will store the name of the filename of where to store output, or from where to get input
    char filename[1000];
    //variable which will be used to store the here string in case of a type 4 operand
    char here_string[1000];
    //array of strings to hold commands for piping
    char *commands_for_pipe[MAX_ARGS];
    //variable to hold whether there is a pipe in the input string
    int has_pipe;
    //token will be used for the strtok, args will be used to hold the arguments from the strtok upon the input command
    char *token = NULL, *args[MAX_ARGS];
    //variable to hold the amount of tokens performed
    int tokenIndex;

    char linecopy[256];

    strcpy(linecopy,line);

    //get type
    op_type = opType(line);
    //check pipe
    has_pipe = hasPipe(line);


    //if has pipe
    if(has_pipe)
    {
        //use of strtok to seperate commands with pipes
        token = strtok(line, "|");

        //loop through tokens
        for (tokenIndex = 0; token != NULL; tokenIndex++)
        {
            //set the commands
            commands_for_pipe[tokenIndex] = token;
            token = strtok(NULL, "|");
        }

        //function to handle pipes
        pipeIt(commands_for_pipe, tokenIndex);

    }

        //if there is no pipes
    else
    {
        //If there is input or output operand
        if(op_type!=0)
        {
            //get index
            op_index = opIndex(line);

            //determine length
            switch(op_type)
            {
                case 1: op_length =1;break;
                case 2: op_length =2;break;
                case 3: op_length =1;break;
                case 4: op_length =3;break;
            }

            //get command before operator
            substring(line, command, 1, op_index);
            //if it is not a here string, hence a filename
            if(op_type!=4)
                substring(line, filename, op_index+op_length+2, strlen(line));
            else // else get here string
                substring(line, here_string, op_index+op_length+3, strlen(line));

            //Removing ' or " character from here string at the end
            if(here_string[strlen(here_string)-1]=='"' || here_string[strlen(here_string)-1]=='\'')
                here_string[strlen(here_string)-1] = '\0';


        }

        //If read input from file
        if(op_type==3)
        {
            doReadInput(filename, command);
        }
        else
        {
            //If no operators
            if (op_type == 0)
            {
                //tokenise string inputted by user
                token = strtok(line, " ");
            }
                //If here string, tokenize here string
            else if (op_type == 4)
            {
                strcat(command, here_string);
                token = strtok(command, " ");
            }
            else
            {
                //tokenise command
                token = strtok(command, " ");
            }

            //populate args with tokenised commands
            for (tokenIndex = 0; token != NULL && tokenIndex < MAX_ARGS - 1; tokenIndex++) {
                args[tokenIndex] = token;
                token = strtok(NULL, " ");
            }

            // set last token to NULL
            args[tokenIndex] = NULL;

            //Removing '\n' character from first argument
            if (args[0][strlen(args[0]) - 1] == '\n')
                args[0][strlen(args[0]) - 1] = '\0';

            //If there are tokens
            if (tokenIndex > 0)
            {
                //If first command is exit
                if (strcmp(args[0], "exit") == 0)
                {
                    exit(0);
                }
                    //If first command is print
                else if (strcmp(args[0], "print") == 0)
                {
                    //if '>' operator
                    if (op_type == 1)
                        doPrintText(args, filename, tokenIndex, 0);
                        //if '>>' operator
                    else if (op_type == 2)
                        doPrintText(args, filename, tokenIndex, 1);
                    else
                        doPrint(args, tokenIndex);

                }
                    //If first command is chdir, to chnage directory
                else if (strcmp(args[0], "chdir") == 0)
                {
                    char oldCWD[1000];
                    //get current work directory
                    getcwd(oldCWD, sizeof(oldCWD));

                    //Removing '\n' character from line read
                    if(args[1][strlen(args[1])-1]=='\n')
                        args[1][strlen(args[1])-1] = '\0';
                    //change directory
                    chdir(args[1]);
                    char newCWD[1000];
                    //get new directory
                    getcwd(newCWD, sizeof(newCWD));
                    //check if the directory moved successfully
                    if (strcmp(oldCWD, newCWD) == 0)
                        printf("Current Working Directly not changed\n");
                    else
                        setenv("CWD", newCWD, 1);
                }
                    //If first command is all
                else if (strcmp(args[0], "all") == 0)
                {
                    //if '>' operator
                    if (op_type == 1)
                        doAllPrint(filename, 0);
                        //if '>>' operator
                    else if (op_type == 2)
                        doAllPrint(filename, 1);
                    else
                        doAll();

                }
                    //If first command is source
                else if (strcmp(args[0], "source") == 0) {
                    //if '>' operator
                    if (op_type == 1)
                        doSourcePrint(args[1], filename, 0);
                        //if '>>' operator
                    else if (op_type == 2)
                        doSourcePrint(args[1], filename, 1);
                    else
                        doSource(args[1]);

                }
                    //If there is only 1 token and has an equals, it must be an assignment to a variable
                else if ((hasEquals(linecopy) == 1))
                {
                    if(tokenIndex == 1)
                    {
                        int dollar_index = getDollarIndex(args[0]);

                        char newValue[1000];
                        char newVariable[1000];


                        if (dollar_index == 0)
                            putenv(args[0]);
                        else {
                            substring(args[0], newVariable, 1, dollar_index);
                            substring(args[0], newValue, dollar_index + 1, strlen(args[0]));
                            char *var = getVariables(newValue);
                            strcat(newVariable, var);
                            putenv(newVariable);
                        }
                    }
                    else
                    {
                        printf("Command Error: Use no spaces. Example: NAME=yourname");
                    }
                }
                    //external command, try fork-and-exec
                else
                {

                    //if '>' operator
                    if (op_type == 1)
                        doForkPrint(args, filename, 0, tokenIndex);
                        //if '>>' operator
                    else if (op_type == 2)
                        doForkPrint(args, filename, 1, tokenIndex);
                    else
                        doFork(args, tokenIndex);
                }


            }
        }
    }
}

/**
 * Method to print to a file
 * @param args: arguments for print
 * @param filename: filename to print to
 * @param numOfArgs: number of arguments
 * @param append: if 1, append to file, else if 0, write at position 0
 */
void doPrintText(char **args, char *filename, int numOfArgs, int append)
{
    FILE * fp;
    int i;

    //open file to write or append to
    if(append==0)
        fp = fopen (filename,"w");
    else
        fp = fopen (filename,"a");

    //variable to hold the variable name without the $ in front
    char var[MAX_ARGS];

    for(int i=1; i<numOfArgs; i++)
    {
        //create a space between each arguments unless first argument
        if(i!=1)
            fprintf(fp, " ");

        //If first character is a $ sign
        if(args[i][0] == '$')
        {
            //get variable name
            substring(args[i], var, 2, strlen(args[i]));

            //remove any new line characters at the end
            if(var[strlen(var)-1]=='\n')
                var[strlen(var)-1] = '\0';

            if(getenv(var)!=NULL)
                fprintf(fp, "%s", getenv(var));
            else
            {
                //Removing '\n' character from line read
                if(args[i][strlen(args[i])-1]=='\n')
                    args[i][strlen(args[i])-1] = '\0';
                fprintf(fp, "%s", args[i]);
            }
        }
        else
        {
            //Removing '\n' character from line read
            if(args[i][strlen(args[i])-1]=='\n')
                args[i][strlen(args[i])-1] = '\0';
            fprintf(fp, "%s", args[i]);
        }
    }
    fprintf(fp, "\n"); //skip line
    fclose(fp); //close file
}

//Method to print, this method does exactly the same as the one above, but prints to the console rather than to a file
void doPrint(char **args, int numOfArgs)
{
    //variable to store variable name
    char var[1000];

    for(int i=1; i<numOfArgs; i++)
    {
        //do space between arguments
        if(i!=1)
            printf(" ");

        //If first character is a $ sign
        if(args[i][0] == '$')
        {
            //get variable name
            substring(args[i], var, 2, strlen(args[i]));

            //remove any new line characters at the end
            if(var[strlen(var)-1]=='\n')
                var[strlen(var)-1] = '\0';

            if(getenv(var)!=NULL)
                printf("%s", getenv(var));
            else
            {
                //Removing '\n' character from line read
                if(args[i][strlen(args[i])-1]=='\n')
                    args[i][strlen(args[i])-1] = '\0';
                printf("%s", args[i]);
            }

        }
        else
        {
            //Removing '\n' character from line read
            if(args[i][strlen(args[i])-1]=='\n')
                args[i][strlen(args[i])-1] = '\0';
            printf("%s", args[i]);
        }

    }
    printf("\n");
}

/**
 * Method to read input from a file
 * @param filename: filename to read from
 * @param command: main command to execute
 */
void doReadInput(char *filename, char *command)
{
    //variable to hold operand type
    int op_type;
    //variable to hold operand index
    int op_index;
    //variable to hold operand length
    int op_length;
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    char *token = NULL, *args[MAX_ARGS];

    //Number of tokens
    int tokenIndex;
    //variable to hold full command, command plus from input from file
    char full_command[1000];
    //in case of an operand in the command inside the file
    char filename_from_file[1000];
    fp = fopen(filename, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    int i=1;
    //for each line in file
    while (getline(&line, &len, fp) != -1)
    {

        //get full command by adding to the command passed
        strcpy(full_command, "");
        strcat(full_command, command);
        strcat(full_command, line);

        performTask(full_command);

    }

    fclose(fp);
}

//Do Fork method
void doFork(char **args, int args_amt)
{
    //variabel to store path
    char PATH[256];
    char CWD[256];
    strcpy(PATH, getenv("PATH"));
    getcwd(CWD, sizeof(CWD));
    int result =0;
    char error_code[10];

    //do fork
    if (fork()== 0)
    {
        //set variables
        setenv("CWD", CWD, 1);
        setenv("TERMINAL", ttyname(STDIN_FILENO), 1);

        //change any varibles into values
        for(int i =0; i<args_amt; i++)
        {
            if(args[i][0]=='$')
            {
                strcpy(args[i],getVariables(args[i]));
            }
        }


        //execute command
        result=execvp(args[0], args);

        if(result==-1)
        {
            perror("Command Error: ");
            sprintf(error_code, "%d", result);
            setenv("EXITCODE", error_code, 1);
        }

        //once ready kill child
        kill(getpid(), SIGKILL);

    }
    else
    {
        //wait for child
        wait(NULL);
    }
}

/**
 * Do Fork but print to file
 * @param args: arguments to pass to execv
 * @param filename: filename to file to read to
 * @param append: if 1, append, if 0, start from pos 0
 */
void doForkPrint(char **args, char *filename, int append, int args_amt)
{
    char PATH[256];
    char CWD[256];
    strcpy(PATH, getenv("PATH"));
    getcwd(CWD, sizeof(CWD));
    //amount of tokens
    int tokenIndex;
    int result =0;
    int fp;
    char error_code[10];

    //do fork
    if (fork()== 0)
    {
        //set variables
        setenv("CWD", CWD, 1);
        setenv("TERMINAL", ttyname(STDIN_FILENO), 1);

        //if 0, start from pos 0
        if(append==0)
            fp = open(filename, O_WRONLY | O_CREAT | O_TRUNC);
            //If 1 append to file
        else
            fp = open(filename, O_RDWR | O_APPEND);

        //redirect output to file
        dup2(fp, STDOUT_FILENO);

        //change any varibles into values
        for(int i =0; i<args_amt; i++)
        {
            if(args[i][0]=='$')
            {
                strcpy(args[i],getVariables(args[i]));
            }
        }

        //execute
        result=execvp(args[0], args);

        //log error
        if(result==-1)
        {
            perror("Command Error: ");
            sprintf(error_code, "%d", result);
            setenv("EXITCODE", error_code, 1);
        }

        //close file pointer
        close(fp);

        //kill child
        kill(getpid(), SIGKILL);

    }
    else
    {
        //wait for child to finish
        wait(NULL);
    }
}


//Method to print all environment variables
void doAll()
{
    printf("PATH=%s\n", getenv("PATH"));
    printf("PROMPT=%s\n", getenv("PROMPT"));
    printf("SHELL=%s\n", getenv("SHELL"));
    printf("USER=%s\n", getenv("LOGNAME"));
    printf("HOME=%s\n", getenv("HOME"));
    printf("CWD=%s\n", getenv("CWD"));
    printf("TERMINAL=%s\n", getenv("TERMINAL"));
}

//Method to print all environment variables to file, to append or start from start
void doAllPrint(char *filename, int append)
{
    FILE * fp;
    int i;

    char newCWD[1000];
    getcwd(newCWD, sizeof(newCWD));

    /* open the file for writing*/
    if(append==0)
        fp = fopen (filename,"w");
    else
        fp = fopen (filename,"a");

    fprintf(fp, "PATH=%s\n", getenv("PATH"));
    fprintf(fp, "PROMPT=%s\n", getenv("PROMPT"));
    fprintf(fp, "SHELL=%s\n", getenv("SHELL"));
    fprintf(fp, "USER=%s\n", getenv("LOGNAME"));
    fprintf(fp, "HOME=%s\n", getenv("HOME"));
    fprintf(fp, "CWD=%s\n", getenv("CWD"));
    fprintf(fp, "TERMINAL=%s\n", getenv("TERMINAL"));

    fclose(fp);

}


/**
 * This method is called when the command source is called to print the output to a text file.
 * @param filename_to_read: filename to read from
 * @param filename_to_write: filename to write from
 * @param append: if 1 append, if 0 start from pos 0
 */
void doSourcePrint(char *filename_to_read, char *filename_to_write, int append)
{
    //file pointer to read
    FILE * fp_read;
    //file pointer to write
    FILE * fp_write;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    //write or append
    if(append==0)
        fp_write = fopen (filename_to_write,"w");
    else
        fp_write = fopen (filename_to_write,"a");

    char *token = NULL, *args[MAX_ARGS];

    //amount of tokens
    int tokenIndex;

    //open file to read
    fp_read = fopen(filename_to_read, "r");

    if (fp_read == NULL)
        exit(EXIT_FAILURE);

    int i=1;

    //loop through each line
    while (getline(&line, &len, fp_read) != -1)
    {
        //seperate each line with spaces
        token = strtok(line, " ");

        //populate arguments
        for (tokenIndex = 0; token != NULL && tokenIndex < MAX_ARGS - 1;  tokenIndex++)
        {
            args[tokenIndex] = token;
            token = strtok(NULL, " ");
        }

        // set last token to NULL
        args[tokenIndex] = NULL;


        //Removing '\n' character from first argument
        if(args[0][strlen(args[0])-1]=='\n')
            args[0][strlen(args[0])-1] = '\0';

        //If there are tokens
        if(tokenIndex>0)
        {
            //If exit
            if(strcmp(args[0], "exit") == 0)
            {
                exit(0);
            }
                //If print
            else if(strcmp(args[0], "print") == 0)
            {

                doPrintText(args, filename_to_write, tokenIndex, 1);

            }
                //If chdir, change current working firectory
            else if(strcmp(args[0], "chdir") == 0)
            {
                char oldCWD[1000];
                //get current working directory
                getcwd(oldCWD, sizeof(oldCWD));
                //change directory
                chdir(args[1]);
                char newCWD[1000];
                //get new working directory
                getcwd(newCWD, sizeof(newCWD));
                //check if the directory changed or not
                if(strcmp(oldCWD, newCWD) == 0)
                    fprintf(fp_write, "Current Working Directly not changed\n");
                else
                    setenv("CWD", newCWD, 1);
            }
                //If all
            else if(strcmp(args[0], "all") == 0)
            {
                doAllPrint(filename_to_write, 1);
            }
                //If source
            else if(strcmp(args[0], "source") == 0)
            {
                doSourcePrint(args[1], filename_to_write, 1);
            }

        }
    }
    //close both file pointers
    fclose(fp_read);
    fclose(fp_write);
}

//Method to execute commands from a file
void doSource(char *filename)
{

    FILE * fp;
    char * line = NULL;
    size_t len = 0;

    //open file
    fp = fopen(filename, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    int i=1;
    //for each line
    while (getline(&line, &len, fp) != -1)
    {
        //execute main method of the program to handle commands
        performTask(line);
    }

    fclose(fp);
}

/**
 * Method which executes piped commands
 * @param commands: commands
 * @param commands_amt: amount of commands
 * @return
 */
void pipeIt(char **commands, int commands_amt)
{
    //process id
    pid_t pid;
    //pipes
    int pipe1[2];
    int pipe2[2];
    //variable to hold path of environment variable
    char PATH[256];
    //variable to hold the path to try
    char try_path[256];
    strcpy(PATH, getenv("PATH"));
    //amount of tokens
    int tokenIndex;
    int paths_amt;
    int result =0;
    int counter=0;
    char *args[MAX_ARGS];
    char *token = NULL, *paths[MAX_ARGS];
    //variable to hold value of error code in string
    char error_code[10];

    char var[1000];

    //tokenise env variable
    token = strtok(PATH, ":");

    //populate oaths
    for (paths_amt = 0; token != NULL && paths_amt < MAX_ARGS - 1;  paths_amt++)
    {
        paths[paths_amt] = token;
        token = strtok(NULL, ":");
    }

    // create pipe1
    if (pipe(pipe1) == -1) {
        perror("bad pipe1");
        exit(1);
    }

    // create pipe2
    if (pipe(pipe2) == -1) {
        perror("bad pipe2");
        exit(1);
    }

    //for each command
    for(int i=0; i < commands_amt; i++)
    {

        //split commands by spaces
        token = strtok(commands[i], " ");

        //populate arguments
        for (tokenIndex = 0; token != NULL && tokenIndex < MAX_ARGS - 1; tokenIndex++) {
            args[tokenIndex] = token;

            //if variable, get variable
            if(args[tokenIndex][0]=='$')
            {
                args[tokenIndex] = getVariables(args[tokenIndex]);

            }
            //remove ' and " from the end
            if(args[tokenIndex][0]=='\'' && args[tokenIndex][strlen(args[tokenIndex])-1]=='\'') {
                substring(args[tokenIndex], var, 2, strlen(args[tokenIndex]) - 2);
                args[tokenIndex] = var;
            }
            token = strtok(NULL, " ");


        }
        //set last argument to NULL
        args[tokenIndex] = NULL;

        //fork
        if ((pid = fork()) == -1) {
            perror("bad fork1");
            exit(1);
        }
            //in child
        else if (pid == 0)
        {
            //if more than 2 piped commands
            if(commands_amt>2)
            {
                //If first pipe
                if (i == 0) {

                    dup2(pipe1[1], 1);
                    // close fds
                    close(pipe1[0]);
                    close(pipe1[1]);
                }
                    //If not first or last pipe
                else if (i != 0 && i != (commands_amt - 1)) {

                    // input from pipe1
                    dup2(pipe1[0], 0);
                    // output to pipe2
                    dup2(pipe2[1], 1);
                    // close fds
                    close(pipe1[0]);
                    close(pipe1[1]);
                    close(pipe2[0]);
                    close(pipe2[1]);
                }
                    //If last pipe
                else if (i == (commands_amt - 1)) {
                    dup2(pipe2[0], 0);

                    //Close fds
                    close(pipe2[0]);
                    close(pipe2[1]);
                }
            }
            else // 2 piped commands
            {
                if (i == 0)
                {
                    dup2(pipe1[1], 1);
                    // close fds
                    close(pipe1[0]);
                    close(pipe1[1]);
                }
                else
                {
                    dup2(pipe1[0], 0);

                    //Close fds
                    close(pipe1[0]);
                    close(pipe1[1]);
                }
            }

            result=0;
            counter=0;

            //try different paths, by concatinating paths from env variable to main command and executing execv until
            //result != -1 and all paths have been searched
            do
            {
                memset(try_path, 0, sizeof(try_path));
                strcpy(try_path, paths[counter]);
                strcat(try_path, "/");
                strcat(try_path, args[0]);
                result = execv(try_path, args);
                sprintf(error_code, "%d", result);
                setenv("EXITCODE", error_code, 1);
                counter++;
            }
            while(result==-1 && counter < paths_amt);
            // exec didn't work, exit
            perror("bad exec ps");
            _exit(1);
        }

        //close unused pipes
        if(i==(commands_amt-2))
        {
            if(commands_amt > 2)
            {
                // close unused fds
                close(pipe1[0]);
                close(pipe1[1]);
            }
        }

        //close pipes at the end
        if(i==(commands_amt-1))
        {
            if(commands_amt >2)
            {
                close(pipe2[0]);
                close(pipe2[1]);
            } else
            {
                close(pipe1[0]);
                close(pipe1[1]);
            }

            //wait for children
            while (wait(NULL) != -1 || errno != ECHILD) {
            }
        }
    }

}

